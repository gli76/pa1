//-----------------------------------------------------------------------------
// Lex.c
// Implementation file for Lex Program
//-----------------------------------------------------------------------------
#include<stdlib.h>
#include<stdio.h>

#include<assert.h>
#include<stdbool.h>
#include<string.h>
#include "List.h"

#define MAX_LEN 300

int main(int argc, char* argv[]){
    int line_count;
    FILE *in, *out;
    char line[MAX_LEN];
    char **str_arr = NULL;
    int size = 20;
    int delta_size = 20;

    List L = newList();

    str_arr = (char**) malloc(size*sizeof(char*));

    char file1[20] = "in1.txt";
    char file2[20] = "outtest.txt";


    //in = fopen(file1, "r");
    in = fopen(argv[1], "r");
    //out = fopen(file2, "w");
    out = fopen(argv[2], "w");

    if(in == NULL){
        printf("Unalbe to open file %s for reading.\n", file1);
        exit(EXIT_FAILURE);
    }

    if(out == NULL){
        printf("Unalbe to open file %s for writing.\n", file1);
        exit(EXIT_FAILURE);
    }


    
    // read each line of of input file
    line_count = 0;
    while(fgets(line, MAX_LEN, in) != NULL){
        line_count++;
        //printf("Line count is %d\n", line_count);
        if(line_count > size)
        {
            size += delta_size;
            char ** new_str_arr = realloc(str_arr, size * sizeof(char*));
            if(new_str_arr == NULL && size != 0){
                puts("Out of memory.");
                exit(EXIT_FAILURE);
            }
            for(int i = 0;i < line_count - 1; i++){
                new_str_arr[i] = str_arr[i];
            }
            str_arr = new_str_arr;
        }
        
        str_arr[line_count - 1] = (char *) malloc(MAX_LEN * sizeof(char));
        strcpy(str_arr[line_count - 1], line);
    }

    int prev;
    int curr;
    for(int i = 0; i < line_count; i++){
        //printf("%s",str_arr[i]);

	//printf("Length is %d\n", length(L));

        if(length(L) == 0){
            prepend(L, i);
	    continue;
        }

        
        if(strcmp(str_arr[i],str_arr[front(L)])<0){
	    //printf("%d inserted before front.\n", i);
            prepend(L, i);
            continue;
        }

        if(strcmp(str_arr[i],str_arr[back(L)])>=0){
            //printf("%d inserted after back.\n", i);
            append(L, i);
            continue;
        }

	moveFront(L);

        prev = get(L);
        moveNext(L);
        curr = get(L);
        while( GetIndex(L) > 0 ){
            if(strcmp(str_arr[i],str_arr[curr])<0)
            {
                insertBefore(L,i);
                //printf("%d inserted.\n", i);
                break;
            }
            else{
                moveNext(L);
            }
        }
    }

    moveFront(L);

    while(GetIndex(L)>=0){
        //printf("%s", str_arr[get(L)]);
	//printf("%d\n", get(L));
	fprintf(out,"%s",str_arr[get(L)]);
	moveNext(L);
    }

    fclose(in);
    fclose(out);
    
    
    return(0);
}
