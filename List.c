//-----------------------------------------------------------------------------
// Stack.c
// Implementation file for List ADT
//-----------------------------------------------------------------------------
#include<stdlib.h>
#include<stdio.h>

#include<assert.h>
#include<stdbool.h>
#include "List.h"

// structs --------------------------------------------------------------------

// private NodeObj type
typedef struct NodeObj{
   ListElement data;
   struct NodeObj* next;
   struct NodeObj* prev;
} NodeObj;

// private Node type
typedef NodeObj* Node;

// private ListObj type
typedef struct ListObj{
   Node front;
   Node back;
   Node cursor;
   int length;
   int index;
} ListObj;


// Constructors-Destructors ---------------------------------------------------

// newNode()
// Returns reference to new Node object. Initializes next and data fields.
Node newNode(ListElement data){
   Node N = malloc(sizeof(NodeObj));
   assert( N!=NULL );
   N->data = data;
   N->next = NULL;
   N->prev = NULL;
   return(N);
}

// freeNode()
// Frees heap memory pointed to by *pN, sets *pN to NULL.
void freeNode(Node* pN){
   if( pN!=NULL && *pN!=NULL ){
      free(*pN);
      *pN = NULL;
   }
}

// newList()
// Returns reference to new empty List object.
List newList(void){
   List L;
   L = malloc(sizeof(ListObj));
   assert( L!=NULL );
   L->front = NULL;
   L->back = NULL;
   L->cursor = NULL;
   L->index = -1;
   L->length = 0;
   return(L);
}

// freeList()
// Frees all heap memory associated with Stack *pL, and sets *pL to NULL.
void freeList(List* pL){
   if(pL!=NULL && *pL!=NULL){
      clear(*pL);
      free(*pL);
      *pL = NULL;
   }
}


// Access functions -----------------------------------------------------------

// length()
// Return the number of elements in L
int length(List L){
   if( L==NULL ){
      printf("List Error: calling length() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   return(L->length);
}

// GetIndex()
// Return index of cursor element if defined, -1 otherwise
int GetIndex(List L){
    if( L == NULL ){
      printf("List Error: calling GetIndex() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   return(L->index);
}


// front()
// Returns the front element of L.
// Pre: length()>0
ListElement front(List L){
   if( L==NULL ){
      printf("List Error: calling front() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( L->length == 0 ){
      printf("List Error: calling front() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   return(L->front->data);
}

// back()
// Returns the back element of L.
// Pre: length()>0
ListElement back(List L){
   if( L==NULL ){
      printf("List Error: calling back() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( L->length == 0 ){
      printf("List Error: calling back() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   return(L->back->data);
}

// get()
// Returns cursor element of L.
// Pre: length()>0, index() > 0
ListElement get(List L){
   if( L==NULL ){
      printf("List Error: calling back() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( L->length == 0 ){
      printf("List Error: calling back() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   return(L->cursor->data);
}

bool equals(List A, List B){
    if( A == NULL || B == NULL ){
      printf("List Error: calling equals() on NULL List reference\n");
      exit(EXIT_FAILURE);
    }

    if( A->length != B->length){
        return(false);
    }
    
    Node node1 = A->front;
    Node node2 = B->front;

    if( A->index != B->index){
        return(false);
    }

    while(node1 != NULL && node2 != NULL){
        if(node1->data != node2->data){
            return(false);
        }
        node1 = node1->next;
        node2 = node2->next;
    }
    return(true);
}


// Manipulation procedures ----------------------------------------------------

// clear()
// Resets L to its original ememty state.
void clear(List L){
    if( L == NULL ){
        printf("List Error: calling back() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    while(L->length != 0){
        deleteFront(L);
    }
}

// set()
// Overwrites the cursor element's data with x
// Pre: length()>0, index()>0
void set(List L, ListElement x){
    if( L == NULL ){
        printf("List Error: calling back() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }
    if( L->length == 0 ){
        printf("List Error: calling back() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    L->cursor->data = x;
}

// moveFront()
// If L is non-empty, sets cursor under the front element, otherwise does nothing
void moveFront(List L){
    if( L == NULL ){
        printf("List Error: calling moveFront() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }
    if( L->length == 0 ){
        printf("List Error: calling moveFront() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    L->index = 0;
    L->cursor = L->front;
}

// moveBack()
// If L is non-empty, sets cursor under the back element, otherwise does nothing
void moveBack(List L){
    if( L == NULL ){
        printf("List Error: calling moveBack() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }
    if( L->length == 0 ){
        printf("List Error: calling moveBack() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    L->index = L->length - 1;
    L->cursor = L->back;
}

// movePrev()
// If cursor is defined and not at front, move cursor one step toward the front 
// of L; if cursor is defined and at font, cursor becomes undefined; if cursor is 
// undefined, do nothing.
void movePrev(List L){
    if( L == NULL ){
        printf("List Error: calling moveBack() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }
    if( L->length == 0 ){
        printf("List Error: calling moveBack() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    if(L->index == -1) return;

    if(L->index == 0){
        L->index = -1;
        L->cursor = NULL;
        return;
    }

    L->cursor = L->cursor->prev;
    L->index = L->index - 1;
}

// moveNext()
// If cursor is defined and not at back, move one step toward the back of L; if
// cursor is defined and at back, cursor becomes undefined; if cursor is undefined
// do nothing
void moveNext(List L){
    if( L == NULL ){
        printf("List Error: calling moveNext() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }
    if( L->length == 0 ){
        printf("List Error: calling moveNext() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    if(L->index == -1) return;

    if(L->index == L->length - 1){
        L->index = -1;
        L->cursor = NULL;
        return;
    }

    L->cursor = L->cursor->next;
    L->index = L->index + 1;
}

// prepend()
// Insert new element into L. If L is non-empty, insertion takes place before 
// front element.
void prepend(List L, ListElement x){
    if( L == NULL ){
        printf("List Error: calling prepend() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    Node N = newNode(x);

    if(L->length == 0){
        L->front = N;
        L->back = N;
        L->index = 0;
        L->length = 1;
    }
    else{
        N->next = L->front;
        L->front->prev = N;
        L->front = N;
        L->length += 1;
        if(L->index != -1)
        {
            L->index += 1;
        }
    }
}

// append()
// Insert new element into L. If L is non-empty, insertion takes place after 
// back element.
void append(List L, ListElement x){
    if( L == NULL ){
        printf("List Error: calling append() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    Node N = newNode(x);

    if(L->length == 0){
        L->front = N;
        L->back = N;
        L->length = 1;
    }
    else{
        L->back->next = N;
        N->prev = L->back;
        L->back = N;
        L->length += 1;
    }
}

// insertBefore()
// Insert new element before cursor.
// Pre: length()>0, index()>0
void insertBefore(List L, ListElement x){
    if( L == NULL ){
        printf("List Error: calling insertBefore() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    Node N = newNode(x);

    if(L->index == -1){
        return;
    }
    if(L->index == 0){
        prepend(L, x);
    }
    else{
        N->next = L->cursor;
        N->prev = L->cursor->prev;
        L->cursor->prev->next = N;
        L->cursor->prev = N;
        L->length += 1;
        L->index += 1;
    }
}

// insertAfter()
// Insert new element after curosr
// Pre: length()>0, index()>0
void insertAfter(List L, ListElement x){
    if( L == NULL ){
        printf("List Error: calling insertAfter() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    Node N = newNode(x);

    if(L->index == -1){
        return;
    }
    if(L->index == L->length - 1){
        append(L, x);
    }
    else{
        N->next = L->cursor->next;
        N->prev = L->cursor;
        L->cursor->next->prev = N;
        L->cursor->next = N;
        L->length += 1;
    }
}

// deleteFront()
// Delete the front element.
// Pre: length()
void deleteFront(List L){
    if( L == NULL ){
        printf("List Error: calling deleteFront() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    if( L->length == 0 ){
        printf("List Error: calling deleteFront() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    Node N = L->front;

    if(L->length == 1){
        L->front = NULL;
        L->back = NULL;
        L->cursor = NULL;
        L->index = -1;
        L->length = 0;
    }
    else{
        L->front = L->front->next;
        L->front->prev = NULL;
        L->length--;
        if(L->index == 0){
            L->index = -1;
            L->cursor = NULL;
        }
        if(L->index > 0){
            L->index--;
        }
    }
    freeNode(&N);
}

// deleteBack()
// Delete the back element
// Pre: length()>0
void deleteBack(List L){
    if( L == NULL ){
        printf("List Error: calling deleteBack() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    if( L->length == 0 ){
        printf("List Error: calling deleteBack() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    Node N = L->back;

    if(L->length == 1){
        L->front = NULL;
        L->back = NULL;
        L->cursor = NULL;
        L->index = -1;
        L->length = 0;
    }
    else{
        L->back = L->back->prev;
        L->back->next = NULL;
        L->length--;
        if(L->index == L->length - 1){
            L->index = -1;
            L->cursor = NULL;
        }
    }
    freeNode(&N);
}


// delete()
// Delete cursor element, making cursor undefined()
// Pre: length(), index()>0
void delete(List L){
    if( L == NULL ){
        printf("List Error: calling delete() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    if( L->length == 0 ){
        printf("List Error: calling delete() on an empty List\n");
        exit(EXIT_FAILURE);
    }

    if( L->index == -1 ){
        printf("List Error: calling deleteCursor() on undefined index\n");
        exit(EXIT_FAILURE);
    }

    if(L->index == 0){
        deleteFront(L);
    }
    else if(L->index == L->length - 1){
        deleteBack(L);
    }
    else{
        Node N = L->cursor;
        N->next->prev = N->prev;
        N->prev->next = N->next;
        L->index = -1;
        L->cursor = NULL;
        L->length--;
        freeNode(&N);
    }
}


// Other Functions ------------------------------------------------------------

// printList()
// Prints to the file pointed to by out, a string representation of L consisting
// of a space separated sequence of integers, with front on left.
void printList(FILE* out, List L){
    Node N = NULL;

    if(out == NULL){
        printf("Unable to open file for writing.");
        exit(EXIT_FAILURE);
    }

    if( L == NULL ){
        printf("List Error: calling printList() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    for(N = L->front; N != NULL; N = N->next){
        fprintf(out, FORMAT" ", N->data);
    }
}

// copyList()
// Returns a new List representing the same integer sequence as L. The cursor in
// the new List is undefined, regardless of the state of the cursor in L. The 
// state of L is unchanged.
List copyList(List L){
    List L2 = newList();
    Node N = NULL;

    if( L == NULL ){
        printf("List Error: calling copyList() on NULL List reference\n");
        exit(EXIT_FAILURE);
    }

    if( L->length == 0 ){
        return L2;
    }
    
    N = L->front;

    while(N != NULL){
        append(L2, N->data);
        N = N->next;
    }
    return(L2);
}

