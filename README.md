# pa1



## Getting started

List.h      Header file for List ADT

List.c      Implementations for functions of List ADT.

ListTest.c  A series of tests for implementations of ListADT

Lex.c       The main program that orders the line of input file.

Makefile    The makefile that compile the lex program.

