//-----------------------------------------------------------------------------
// List.h
// Header file for List ADT
//-----------------------------------------------------------------------------
#ifndef LIST_H_INCLUDE_
#define LIST_H_INCLUDE_
#include<stdbool.h>



#define FORMAT "%d"  // format string for ListElement

// Exported Types -------------------------------------------------------------
typedef int ListElement;
typedef struct ListObj* List;


// Constructors-Destructors ---------------------------------------------------

// newList()
// Returns reference to new empty List object.
List newList(void);

// freeList()
// Frees all heap memory associated with List *pS, and sets *pS to NULL.
void freeList(List* pL);


// Access functions -----------------------------------------------------------

// length()
// Return the number of elements in L
int length(List L);

// GetIndex()
// Return index of cursor element if defined, -1 otherwise
int GetIndex(List L);

// front()
// Returns the front element of L.
// Pre: length()>0
ListElement front(List L);

// back()
// Returns back element of L.
// Pre: length()>0
int back(List L);

// get()
// Returns cursor element of L.
// Pre: length()>0, index() > 0
ListElement get(List L);

// equals()
// Returns true iff Lists A and B are in same state, and returns false otherwise
bool equals(List A, List B);



// Manipulation procedures ----------------------------------------------------

// clear()
// Resets L to its original ememty state.
void clear(List L);

// set()
// Overwrites the cursor element's data with x
// Pre: length()>0, index()>0
void set(List L, ListElement x);

// moveFront()
// If L is non-empty, sets cursor under the front element, otherwise does nothing
void moveFront(List L);

// moveBack()
// If L is non-empty, sets cursor under the back element, otherwise does nothing
void moveBack(List L);

// movePrev()
// If cursor is defined and not at front, move cursor one step toward the front 
// of L; if cursor is defined and at font, cursor becomes undefined; if cursor is 
// undefined, do nothing.
void movePrev(List L);

// moveNext()
// If cursor is defined and not at back, move one step toward the back of L; if
// cursor is defined and at back, cursor becomes undefined; if cursor is undefined
// do nothing
void moveNext(List L);

// prepend()
// Insert new element into L. If L is non-empty, insertion takes place before 
// front element.
void prepend(List L, ListElement x);

// append()
// Insert new element into L. If L is non-empty, insertion takes place after 
// back element.
void append(List L, ListElement x);

// insertBefore()
// Insert new element before cursor.
// Pre: length()>0, index()>0
void insertBefore(List L, ListElement x);

// insertAfter()
// Insert new element after curosr
// Pre: length()>0, index()>0
void insertAfter(List L, ListElement x);

// deleteFront()
// Delete the front element.
// Pre: length()
void deleteFront(List L);

// deleteBack()
// Delete the back element
// Pre: length()>0
void deleteBack(List L);

// delete()
// Delete cursor element, making cursor undefined()
// Pre: length(), index()>0
void delete(List L);


// Other Functions ------------------------------------------------------------

// printList()
// Prints to the file pointed to by out, a string representation of L consisting
// of a space separated sequence of integers, with front on left.
void printList(FILE * out, List L);

// copyList()
// Returns a new List representing the same integer sequence as L. The cursor in
// the new List is undefined, regardless of the state of the cursor in L. The 
// state of L is unchanged.
List copyList(List L);

// concatList()
// Returns a new List which is the concatenation of A and B. The cursor in the
// new List is undefined, regardless of the states of the cursors in A and B.
// The states of A and B are unchanged.
List concatList(List A, List B);

#endif
