CC=gcc

lex: Lex.o List.o
	$(CC) -std=c17 -Wall -o lex Lex.o List.o

.PHONY: clean

clean:
	rm -f *.o lex
